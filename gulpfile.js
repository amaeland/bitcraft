// Read config (config.json, .bowerrc / bower.json)
var config = require('./config.json');
var glob = config.paths.glob;
var bowerConfig = require('bower-config');
var path = require('path');
config.paths.bower = path.join('./', (bowerConfig.read('./').directory || 'bower_components'));

var gulp = require('gulp');
var wiredep = require('wiredep').stream;
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var argv = require('yargs').argv;
var del = require('del');
var merge = require('merge-stream');
var $ = require('gulp-load-plugins')();
var PROD = !!(argv.prod);
var WATCH = false;

// Handle errors
function handleError(err) {
	console.log(err.toString());
	if (WATCH) {
		this.emit('end');
	}
	else {
		process.exit();
	}
}

// Watch "gulp watch"
gulp.task('watch', ['wiredep'], function() {
	WATCH = true;

	browserSync({
		proxy: config.server.proxy,
		open: config.server.open,
		online: config.server.online
	});

	gulp.watch(config.paths.src.styles + glob + '.scss', ['sass']);
	gulp.watch(config.paths.src.templates + glob + '.html').on('change', reload);
	gulp.watch(config.paths.bower + glob, ['wiredep']).on('change', reload);
	gulp.watch(config.paths.src.scripts + glob + '.js', ['copy:scripts']);

	gulp.watch(config.paths.src.fonts + glob + '{.ttf,.eot,.svg,.woff,.otf}', ['copy:fonts']);
	gulp.watch(config.paths.src.images + glob + '{.png,.jpg,.gif,.svg}', ['copy:images']);

});

// Sass (don't run clean when watching)
gulp.task('sass', WATCH ? [] : ['clean'], function() {
	gulp.src(config.paths.src.styles + glob + '.scss')
		.pipe($.if(!PROD, $.sourcemaps.init()))
		.pipe($.sass({
			includePaths: config.paths.sass.includePaths
		}))
		.on('error', handleError)
		.pipe($.if(!PROD, $.sourcemaps.write()))
		.pipe($.if(PROD, $.autoprefixer(config.autoprefixer.targets)))
		.pipe($.if(PROD, $.minifyCss()))
		.pipe($.if(PROD, $.combineMediaQueries()))
		.pipe(gulp.dest(config.paths.dest.styles))
		.pipe(reload({stream: true}));
});

// Copy javascripts (only used for dev)
gulp.task('copy:scripts', function() {
	return gulp.src(config.paths.src.scripts + glob + '.js')
		.pipe(gulp.dest(config.paths.dest.scripts))
		.pipe(reload({stream: true}));
});

// Copy bower components (only used for dev)
gulp.task('copy:bower', ['sass'], function() {
	return gulp.src(config.paths.bower + glob, { base: config.paths.src.assets.dev })
		.pipe(gulp.dest(config.paths.dest.assets));
});

// Copy images and minify
gulp.task('copy:images', function() {
	return gulp.src(config.paths.src.images + glob + '{.png,.jpg,.gif,.svg}')
		.pipe($.if(PROD, $.imagemin({
			progressive: true,
			interlaced: true,
			svgoPlugins: [{cleanupIDs: false}]
		})))
		.pipe(gulp.dest(config.paths.dest.images));
});

// Generate the src array for grabbing fonts
var fonts = [];
for (var i = 0, len = config.bower.fonts.length; i < len; i++) {
	fonts[i] = config.bower.fonts[i] + glob + '{.ttf,.eot,.svg,.woff,woff2,.otf}';
}
fonts.push(config.paths.src.fonts + glob + '{.ttf,.eot,.svg,.woff,woff2,.otf}');

// Copy fonts
gulp.task('copy:fonts', function() {
	return gulp.src(fonts)
		.pipe(gulp.dest(config.paths.dest.fonts));
});

gulp.task('wiredep', PROD ? ['sass'] : ['copy:bower'], function () {
	return gulp.src(config.paths.src.templates + glob + '.html')
		.pipe(wiredep({
			ignorePath: /^(\/|\.+(?!\/[^\.]))+\.+/, //remove relative path
			exclude: config.bower.excludes,
			overrides: config.bower.overrides
		}))
		.pipe(gulp.dest(config.paths.src.templates));
});

//Build for production
gulp.task('build:useref', ['wiredep'], function () {
	var assets = $.useref.assets({searchPath: ['./templates/', './public/']});

	return gulp.src(config.paths.src.templates + glob + '.html')
		.pipe(assets)
		.pipe($.if('*.js', $.uglify()))
		.pipe($.if('*.css', $.minifyCss()))
		.pipe(assets.restore())
		.pipe($.useref())
		.pipe(gulp.dest(config.paths.dest.templates));
});

//Build for production
gulp.task('build', ['build:useref'], function() {
	return gulp.src(config.paths.src.assets.prod + glob)
		.pipe(gulp.dest(config.paths.dest.assets));
});

//Let's build! (use "gulp --prod" for production)
gulp.task('default', PROD ? ['build'] : ['wiredep'], function(cb) {
	if(!PROD) {
		gulp.start('copy:scripts', 'copy:images', 'copy:fonts');
		del([config.paths.src.assets.prod + glob, config.paths.src.assets.prod], cb);
	} else {
		gulp.start('copy:images', 'copy:fonts');
		del([config.paths.src.assets.prod + glob, config.paths.src.assets.prod,
			config.paths.dest.styles + glob, config.paths.dest.styles], cb);
	}
});

//run "gulp clean" manually when developing - in order to delete craft/templates & public/assets folders/content
gulp.task('clean', function(cb) {
		del([config.paths.dest.assets + glob, config.paths.dest.assets,
			config.paths.dest.templates + glob, config.paths.dest.templates], cb);
});
