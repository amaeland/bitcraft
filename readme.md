Bitcraft - a Craft CMS boilerplate
==============================
Inspired by [Jake Chapmans boilerplate](https://github.com/imjakechapman/CraftCMS-Boilerplate)

### Craft installation
1. Download [Craft](https://buildwithcraft.com/) and copy the /craft/app folder into your projects /craft folder.
2. Then create a local database 
3. Create db.php and general.php files within /craft/config/local/

( I recommend [homestead](http://laravel.com/docs/4.2/homestead) for local dev )
  
/craft/config/db.php uses environment variables as default. If you are using homestead just add `DB_HOST` `DB_USER` and `DB_PASS` to your homestead.yaml file through `homestead edit`.  
  
If you prefer to not use environment variables for local dev - add your local dev credentials to /craft/config/local/db.php  
  
Now that you've put the correct database credentials in db.php, browse to http://localhost/admin and start the craft install process.  
  
### Gulp installation

**Prerequisites for building**  
- [Node](https://nodejs.org)  
- `npm install -g gulp`  to install Gulp globally  
- `npm install -g bower` to install Bower globally
- `npm install` installs gulp/node modules defined in package.json  
- `bower install` installs bower modules defined in bower.json  

Skip the above steps if you already have node, bower and gulp installed.  
Not sure? Run `npm -v`, `gulp -v` and `bower -v`  in your terminal.
  
**Now let's use it!**  
- `gulp` to build for dev  
- `gulp watch` for browser-sync awesomeness  
- `gulp --prod` to build for production  

**IMPORTANT: make sure you have the .bowerrc and .gitignore files in your folder, under certain conditions these are not cloned to your local repository**
  
### Templates & folder structure  
While running on http://localhost, or any http://*.dev - Craft will use the root /templates folder, otherwise the /craft/templates folder will be used. ( Look at [public/index.php](public/index.php) lines:9 to 12 for implementation )
  
Include your app css & js by adding them to the [templates/_modular/_base.html](templates/_modular/_base.html) twig template. (bower components will be added automatically, unless you add an override to exclude them)

** IMPORTANT: always work on the /templates folder. craft/templates and public/assets will be deleted when building **

#### Project folder structure  
```
.  
├── craft  
│   ├── app  
│   ├── config  
│   │   ├── local  
│   │   └── redactor  
│   ├── plugins  
│   ├── storage  
│   └── templates  
├── public  
│   └── assets
└── templates  
    ├── _modular  
    ├── _partials  
    └── assets  
        ├── bower_components  
        ├── fonts  
        ├── images  
        ├── scripts  
        └── styles  
```

Templates and minimised assets from the /templates folder, will be written to their respective folders: `/craft/templates` and `/public/assets` when running `gulp --prod`  

### Configuration 
All you'r configuration goes into the config.json. This is beneficial because it's keeping tasks DRY and config.json can be used by another task runner (ie: grunt).  
  
#### Bower  
Use bower for installing modules, ie.:
  
`bower install jquery --save`  
`bower install susy --save-dev`  
  
You can also add bower excludes in config.json through `bower.wireDepExcludes`. 

Or if a bower module is outdated and you need to override the location of main js/css file, you can use `bower.wireDepOverrides`. (you can also place overrides in bower.json if you prefer that)

Bower font modules should also be added here. If you prefer adding fonts manually just copy them to the templates/assets/fonts folder.
  
```json
"bower" : {
	"exclude" : [
		"bower_components/modernizr/modernizr.js"
	],
	"overrides" : {
		"retinajs" : { "main": "dist/retina.min.js" }
	},
	"fonts" : [
		"templates/assets/bower_components/font-awesome/fonts"
	]
}
```
  
#### Autoprefixer
Add you'r targeted browsers, look at [browserslist](https://github.com/ai/browserslist) for a complete list of possible targets.
```json
"autoprefixer" : {
	"targets" : ["last 2 version", "ie 9"]
}
```

#### Paths
  
Most gulp builds use the `/**/*` glob.  
This build setup uses the `/{,*/}*` glob which will only traverse the first set of sub-folders, which is more efficient. If you need to support all subsequent sub-folders, just replace the glob in config.json with `/**/*`.

**Example:**  
```json
"paths" : {
	"glob" : "/{,*/}*",
	"sass" : {
		"includePaths" : [
		]
	},
	"src" : {
		"templates" : "templates",
		"styles" : "templates/assets/styles",
		"scripts" : "templates/assets/scripts",
		"images" : "templates/assets/images",
		"fonts" : "templates/assets/fonts",
		"prodassets" : "craft/templates/assets"
	},
	"dest" : {
		"templates" : "craft/templates",
		"styles" : "public/assets/css",
		"scripts" : "public/assets/js",
		"images" : "public/assets/images",
		"fonts" : "public/assets/fonts",
		"prodassets" : "public/assets"
	}
}
```
Most of these are self-explanatory, remember to use absolute path for any SASS libraries you might include. For example susy would be included by adding `"includePaths" : ["./templates/assets/bower_components/susy/sass/"]`

#### Server
Setup server params for working with [browsersync](http://www.browsersync.io/)
If you need features of BrowserSync (such as `xip` & `tunnel`) that require an internet connection, set online to true.

```json
"server" : {
	"proxy" : "yourdomain.dev",
	"open" : "false",
	"online" : "false"
}
```