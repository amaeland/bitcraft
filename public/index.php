<?php

// Path to your craft/ folder
$craftPath = '../craft';

// need to access the craft_environment, so define it here
defined('CRAFT_ENVIRONMENT') || define('CRAFT_ENVIRONMENT', $_SERVER['SERVER_NAME']);

if (strpos(CRAFT_ENVIRONMENT, '.dev') !== false || strpos(CRAFT_ENVIRONMENT, 'localhost') !== false) {
  // For local dev, we move templates path to right above web root
  define('CRAFT_TEMPLATES_PATH', realpath(dirname(__FILE__) . "/../templates").'/');
}

// Do not edit below this line
$path = rtrim($craftPath, '/').'/app/index.php';

if (!is_file($path))
{
	exit('Could not find your craft/ folder. Please ensure that <strong><code>$craftPath</code></strong> is set correctly in '.__FILE__);
}

require_once $path;
